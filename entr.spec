Name:           entr
Version:        5.6
Release:        1
Summary:        Run arbitrary commands when files change

License:        ISC
URL:            http://eradman.com/%{name}project/
Source0:        http://eradman.com/%{name}project/code/%{name}-%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  make

%description
A file watcher, which can run specified commands
when target files change.


%prep
%autosetup -n %{name}-%{version} -p1
ln -s Makefile{.linux,}

%build
export CFLAGS="${RPM_OPT_FLAGS}"
export LDFLAGS="%{?__global_ldflags}"
%make_build

%install
export PREFIX=%{_prefix}
%make_install

%check
make test

%files
%license LICENSE
%doc NEWS README.md
%{_bindir}/entr
%{_mandir}/man1/entr.1*

%changelog
* Fri Nov 01 2024 xu_ping <707078654@qq.com> - 5.6-1
- Upgrade to 5.6

* Thu Feb 29 2024 tenglei <tenglei@kylinos.cn> - 5.5-1
- Upgrade to 5.5

* Sun Oct 08 2023 yaoxin <yao_xin001@hoperun.com> - 5.4-1
- Upgrade to 5.4

* Sat Jul 29 2023 xu_ping <707078654@qq.com> -5.0-3
- fix build error due to glibc upgrade

* Wed Aug 24 2022 caodongxia <caodongxia@h-partners.com> -5.0-2
- Add debug package to add strip

* Sun Aug 29 2021 Leo <clouds@isrc.iscas.ac.cn> - 5.0-1
- first publish of version 5.0

